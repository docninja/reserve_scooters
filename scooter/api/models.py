from django.db import models
from django.urls import reverse


class Scooter_Info(models.Model):
    scooter_id = models.AutoField(primary_key=True)
    lng = models.FloatField(default=0.0)
    lat = models.FloatField(default=0.0)
    bin_lat = models.IntegerField(default=0)
    bin_lng = models.IntegerField(default=0)

    class Meta:
        ordering = ['scooter_id']

    def __str__(self):
        return f"{self.json()}"

    def get_absolute_url(self):
        return reverse('scooter-info', args=[str(self.scooter_id)])

    def json(self):
        return {"id": self.scooter_id, "lng": self.lng, "lat": self.lat}


class Reserve_Scooter_Record(models.Model):
    scooter_id=models.IntegerField(primary_key=True)

    class Meta:
        ordering = ['scooter_id']

    def get_absolute_url(self):
        return reverse('is-scooter-reserved', args=[str(self.scooter_id)])
