import os
import json
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from api.common.lib.crud_ops import CRUDOps
from api.common.lib import (
 constants,
)

os.environ[constants.KB_BIN_SIZE] = "10"
os.environ[constants.KB_SIZE] = "100"

crudops=CRUDOps()

@require_http_methods(["GET"])
def reserve(request):
    ''' Method to reserve scooter '''
    scooter_id = str(request.GET.get('id'))
    check=crudops.reserve_scooter(scooter_id)
    return HttpResponse(str({"result":check}))

@require_http_methods(["GET"])
def get_test(request):
    ''' Method which helps test fetching scooter '''
    return HttpResponse(str(crudops.get_scooter(request.GET.get("id"))))

@require_http_methods(["GET"])
def get_reserved(request):
    ''' Get Scooters which are reserved '''
    return HttpResponse(crudops.get_reserved())

@require_http_methods(["GET"])
def available(request):
    ''' Get Available Scooters based on input (lat, lng, radius) '''
    lat = float(request.GET.get('lat'))
    lng = float(request.GET.get('lng'))
    radius = float(request.GET.get('radius'))
    scooters = crudops.get_scooters(lng, lat, radius)
    return HttpResponse(json.dumps(scooters, indent=10))

@require_http_methods(["POST"])
def setvars(request):
    ''' set vars for defining kb '''
    os.environ[constants.KB_BIN_SIZE] = str(request.POST.get('kb_bin_size'))
    os.environ[constants.KB_SIZE] = str(request.POST.get('kb_size'))
    return HttpResponse("set_vars")

@require_http_methods(["POST"])
def loadfixturedata(request):
    ''' run fixture data loading '''
    size = request.POST.get('size')
    for x in range(0, size):
        for y in range(0, size):
            crudops.add_scooter(x, y)
    return HttpResponse("Run successfully loading new fixture data {size}")





