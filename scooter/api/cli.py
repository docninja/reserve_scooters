import argparse
from src.lib import constants
from src.lib.geometry import GeoLogic
from src.lib.dbops import DBOps
from src.lib.crud_ops import CRUDOps
from src.lib.data_process import DataProcess

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    command_help=f'command 1) {constants.INSTALL_DB} ' \
                 f'2) {constants.DESTROY_DB} ' \
                 f'3 ) {constants.LOAD_DB} ' \
                 f'4) {constants.GET_SCOOTER} ' \
                 f'5) {constants.RESERVE_SCOOTER}'
    parser.add_argument(f"{constants.COMMAND}",
                        help=command_help)
    parser.add_argument(f"--{constants.DIR_PATH}",
                        help=f"{constants.DIR_PATH}",
                        default="configs/data.txt")
    parser.add_argument(f"--{constants.LAT}",
                        help=f"{constants.LAT}")
    parser.add_argument(f"--{constants.LNG}",
                        help=f"{constants.LNG}")
    parser.add_argument(f"--{constants.RADIUS}",
                        help=f"{constants.RADIUS}")
    parser.add_argument(f"--{constants.Id}",
                        help=f"scooter Id")
    args = vars(parser.parse_args())
    dbops=DBOps()
    if args.get(f"{constants.COMMAND}") == constants.INSTALL_DB:
        dbops.initialize_db()
    elif args.get(f"{constants.COMMAND}") == constants.DESTROY_DB:
        dbops.destroy_db()
    elif args.get(f"{constants.COMMAND}") == constants.LOAD_DB:
        kbops = GeoLogic()
        dataprocess = DataProcess(dbops=dbops, kbops=kbops)
        crudOps = CRUDOps(dbops=dbops, kbops=kbops)
        scooters=DataProcess().process_files(args.get(f"{constants.DIR_PATH}"))
        crudOps.add_scooters(scooters)
    elif args.get(f"{constants.COMMAND}") == constants.GET_SCOOTER:
        kbops = GeoLogic()
        dataprocess = DataProcess(dbops=dbops, kbops=kbops)
        crudOps = CRUDOps(dbops=dbops, kbops=kbops)
        lng=float(args.get(f"{constants.LNG}"))
        lat = float(args.get(f"{constants.LAT}"))
        radius = float(args.get(f"{constants.RADIUS}"))
        print(crudOps.get_scooters(lng, lat, radius))
    elif args.get(f"{constants.COMMAND}") == constants.RESERVE_SCOOTER:
        kbops = GeoLogic()
        crudOps = CRUDOps(dbops=dbops, kbops=kbops)
        print(dbops.reserve_scooter(args[constants.Id]))
        print(dbops.reserve_scooter(args[constants.Id]))
    else:
        raise Exception(" Command not excepted !")
