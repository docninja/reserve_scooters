
''' Class to define location of object '''

class Location:
    def __init__(self, lng, lat):
        self.lng=lng
        self.lat=lat

    def __str__(self):
        return f"({self.lng},{self.lat})"

    def get_data(self):
        return (self.lng, self.lat)

    def distance(self, loc):
        return ((self.lat-loc.lat)**2+
            (self.lng-loc.lng)**2)

    def is_in_range(self, loc, radius):
        return self.distance(loc) <= (radius*radius)