
''' Class that represents scooter '''

class Scooter:
    def __init__(self, location):
        self.location = location
        self.id=None
        self.bin=None

    def __str__(self):
        return (f"id:: {self.id}, location:: {self.location}, bin = {self.bin}")

    def express_json(self):
        return {"id": self.id, "lng": self.location.lng, "lat": self.location.lat}

    def is_in_range(self, location, radius):
        return self.location.is_in_range(location, radius)

    def add_bin(self, location):
        self.bin=location

    def add_id(self, id):
        self.id = id