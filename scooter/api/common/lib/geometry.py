from api.common.lib.location import Location
from api.common.lib.locations import Locations
from api.common.lib import env_utils

class GeoLogic:
    def __init__(self):
        self.max_size = env_utils.ENVOps.get_kb_size()
        self.min_size = -env_utils.ENVOps.get_kb_size()
        self.bin_size = env_utils.ENVOps.get_kb_bin_size()
        self.get_all_bins()

    def get_bin(self, location):
        return Location(self.get_key(location.lng), self.get_key(location.lat))

    def get_key(self, i):
        return int(round(float(i) / self.bin_size))

    def get_locations(self, location, radius):
        l1 = self.optimize(Location(location.lng - radius, location.lng - radius))
        l2 = self.optimize(Location(location.lng + radius, location.lng + radius))
        l3 = self.optimize(Location(location.lng + radius, location.lng - radius))
        l4 = self.optimize(Location(location.lng - radius, location.lng + radius))
        return (l1, l2, l3, l4)

    def optimize(self, location):
        location.lng = self.optimize_value(location.lng)
        location.lat = self.optimize_value(location.lat)
        return location

    def optimize_value(self, value):
        if value > self.max_size:
            return self.max_size
        elif value < self.min_size:
            return self.min_size
        return value

    def get_bins(self, location, radius):
        bin_info=[]
        possible_bins=[]
        for l in self.get_locations(location, radius):
            bin_info.append(self.get_bin(l))
        x_range = [loc.lng for loc in bin_info]
        y_range = [loc.lat for loc in bin_info]
        for x in range(min(x_range), max(x_range)+1):
            for y in range(min(y_range), max(y_range) + 1):
                possible_bins.append(Location(x, y))
        return possible_bins

    def get_all_bins(self):
        self.bins=Locations()
        for x in range(self.min_size, self.max_size, self.bin_size):
            for y in range(self.min_size, self.max_size):
                self.bins.add_location(Location(x, y))

