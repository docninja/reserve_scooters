
''' Class which represents collection of locations '''

class Locations:
    def __init__(self):
        self.locations=[]

    def add_location(self, location):
        self.locations.append(location)

    def __str__(self):
        data=""
        for location in self.locations:
            data+= f"\n{location}"
        return data

    def filter(self, location, radius):
        return [loc
                    for loc in self.locations
                        if loc.is_in_range(location, radius)
                ]