
''' Class to define collection of scooters '''

class Scooters:
    def __init__(self):
        self.scooters=[]

    def add_scooter(self, scooter):
        self.scooters.append(scooter)

    def __str__(self):
        data=""
        for scooter in self.scooters:
            data+=f"\n{scooter}"
        return data

    def filter(self, location, radius, is_json):
        return [ (scooter.express_json() if is_json else scooter)
                    for scooter in self.scooters
                        if scooter.is_in_range(location, radius)
        ]