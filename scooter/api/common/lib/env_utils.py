from api.common.lib import constants
import os

class ENVOps:

    @staticmethod
    def get_db_user():
        return os.environ.get(constants.DB_USER,
                              constants.DEFAULT_PARAMS[constants.DB_USER])

    @staticmethod
    def get_db_password():
        return os.environ.get(constants.DB_PASSWORD,
                              constants.DEFAULT_PARAMS[constants.DB_PASSWORD])

    @staticmethod
    def get_db_host():
        return os.environ.get(constants.DB_HOST,
                              constants.DEFAULT_PARAMS[constants.DB_HOST])

    @staticmethod
    def get_db_port():
        return os.environ.get(constants.DB_PORT,
                              constants.DEFAULT_PARAMS[constants.DB_PORT])

    @staticmethod
    def get_kb_size():
        return int(os.environ.get(constants.KB_SIZE,
                              constants.DEFAULT_PARAMS[constants.KB_SIZE]))

    @staticmethod
    def get_kb_bin_size():
        return int(os.environ.get(constants.KB_BIN_SIZE,
                              constants.DEFAULT_PARAMS[constants.KB_BIN_SIZE]))