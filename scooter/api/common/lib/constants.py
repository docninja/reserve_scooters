KB_BIN_SIZE="BIN_SIZE"
KB_SIZE="KB_SIZE"
DB_HOST="DB_HOST"
DB_USER="DB_USER"
DB_PASSWORD="DB_PASSWORD"
DB_PORT="DB_PORT"
DB_DBNAME="SCOOTER_INFO"
COMMAND="command"
DIR_PATH="path"
LNG="longitude"
LAT="latitude"
RADIUS="radius"
Id="id"
DEFAULT_PARAMS={
  KB_BIN_SIZE: 2,
  KB_SIZE: 1000,
  DB_HOST: "127.0.0.1",
  DB_PASSWORD: "",
  DB_USER: "root",
  DB_PORT: 3306
}
INSTALL_DB="install_db"
DESTROY_DB="destroy_db"
GET_SCOOTER="get_scrooter"
RESERVE_SCOOTER="reserve_scrooter"
LOAD_DB="load_db"
DB_DEFINITION_FILE_PATH="configs/install.sql"
DROP_SCHEMA_SQL=f"drop schema if exists {DB_DBNAME};"