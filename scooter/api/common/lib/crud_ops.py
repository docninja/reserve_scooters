from api.common.lib.geometry import GeoLogic
from api.common.lib.location import Location
from api.common.lib.scooters import Scooters
from api.common.lib.scooter import Scooter
from api.models import Scooter_Info, Reserve_Scooter_Record

''' Class defines operations for doing CRUD related to application '''

class CRUDOps:
    def __init__(self, kbops=None):
        self.kbops=(kbops if kbops else GeoLogic())

    def get_scooters(self, lng, lat, radius):
        results=[]
        scooters=Scooters()
        for bin in self.kbops.get_bins(Location(lng, lat), radius):
            results+=[data
                      for data in
                      Scooter_Info.objects.raw(f" SELECT * from API_SCOOTER_INFO "
                                               f" where bin_lat = {bin.lat} and bin_lng "
                                               f" = {bin.lng} and scooter_id not in "
                                               f" ( Select scooter_id from API_Reserve_Scooter_Record) ")]
        for result in results:
            print(result)
            scooter=Scooter(Location(result.lng, result.lat))
            scooter.add_id(result.scooter_id)
            scooters.add_scooter(scooter)
        return scooters.filter(Location(lng, lat), radius, True)


    def get_scooter(self, id):
        return Scooter_Info.objects.get(scooter_id=id)

    def get_reserved(self):
        records=[]
        scooters = Scooter_Info.objects.raw(
            " SELECT * from API_SCOOTER_INFO where scooter_id in ( Select scooter_id from API_Reserve_Scooter_Record)")
        for data in scooters:
            records.append(data)
        return records


    def reserve_scooter(self, id):
        print(f" reserving scooter with id :: {id}")
        try:
            p, was_created = Reserve_Scooter_Record.objects.get_or_create(scooter_id=id)
            if was_created:
                return True
            else:
                return False
        except Exception as ex:
            return False

    def delete_scooter_reservation(self, id):
        print(f" removing reserving scooter with id :: {id}")
        model = Reserve_Scooter_Record()
        model.scooter_id=id
        return model.delete()

    def add_scooter(self, lng, lat):
        scooter=Scooter(Location(lng, lat))
        scooter.add_bin(self.kbops.get_bin(scooter.location))
        print(f" adding scooter :: {scooter}")
        model=Scooter_Info()
        model.lng=scooter.location.lng
        model.lat=scooter.location.lat
        model.bin_lat=scooter.bin.lat
        model.bin_lng=scooter.bin.lng
        model.save()