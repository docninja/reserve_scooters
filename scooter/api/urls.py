from . import views
from django.urls import path

urlpatterns = [
    path('api/v1/scooters/loadfixturedata/', views.loadfixturedata),
    path('api/v1/scooters/setvars/', views.setvars),
    path('api/v1/scooters/available/', views.available),
    path('api/v1/scooters/reserve/', views.reserve),
    path('api/v1/scooters/gettest/', views.get_test),
    path('api/v1/scooters/getreserved/', views.get_reserved),
]