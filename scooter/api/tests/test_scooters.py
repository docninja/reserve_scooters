import unittest

from api.common.lib.scooters import Scooters
from api.common.lib.scooter import Scooter
from api.common.lib.location import Location

class TestLocation(unittest.TestCase):

    def test_sql_1(self):
        scooters=Scooters()
        id=0
        for x in range(0,2):
            for y in range(0, 2):
                scooter=Scooter(Location(x, y))
                scooter.add_id(id)
                scooter.add_bin(Location(x, y))
                scooters.add_scooter(scooter)
                id+=1
        print(scooters)

if __name__ == '__main__':
    unittest.main()