import unittest

from api.common.lib.scooter import Scooter
from api.common.lib.location import Location

class TestLocation(unittest.TestCase):

    def test_sql_1(self):
        scooter=Scooter(Location(0,0))
        scooter.add_id((1))
        scooter.add_bin(Location(0,0))
        self.assertTrue(str(scooter) == "id:: 1, location:: (0,0), bin = (0,0)")
        self.assertTrue(scooter.express_json() == {'id': 1, 'lng': 0, 'lat': 0})

if __name__ == '__main__':
    unittest.main()