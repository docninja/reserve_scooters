import unittest

from api.common.lib.location import Location

class TestLocation(unittest.TestCase):

    def test_sql_1(self):
        loc=Location(1,2)
        self.assertTrue(loc.lng == 1)
        self.assertTrue(loc.lat == 2)
        self.assertTrue(str(loc) == "(1,2)")

    def test_sql_dist_1(self):
        loc=Location(1,2)
        loc1 = Location(1, 2)
        self.assertTrue(loc.distance(loc1) == 0)

    def test_sql_dist_2(self):
        loc=Location(1,2)
        loc1 = Location(2, 1)
        self.assertTrue(loc.distance(loc1) == 2)

    def test_sql_dist_3(self):
        loc=Location(0,0)
        loc1 = Location(2, 2)
        self.assertTrue(loc.distance(loc1) == 8)

if __name__ == '__main__':
    unittest.main()