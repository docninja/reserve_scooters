from api.models import Scooter_Info
from django.test import TestCase

import unittest

class TestModels(TestCase):
    def setUp(self):
        Scooter_Info.objects.create(lng=10, lat=10, bin_lat=10, bin_lng=10)
        Scooter_Info.objects.create(lng=20, lat=20, bin_lat=20, bin_lng=20)

    def test_sql_1(self):
        print(Scooter_Info.objects.get(scooter_id=1).json())
        print(Scooter_Info.objects.get(scooter_id=2).json())

if __name__ == '__main__':
    unittest.main()