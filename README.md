# reserve_scooters

#*Assumptions*

	1. Works with python > 3.6
	2. Uses python packages:
   		- mysqlclient
   		- django

#*Directory Description*:

	- django project: scooter/scooter 
	- django app: scooter/api
	- django app tests: scooter/api/tests
	- django app lib: scooter/api/common/lib

#*Design Description*
	1. We use standard KD Tree Algorithm: https://en.wikipedia.org/wiki/K-d_tree
	   defined by kb_bin_size and kb_size for bin allocation and organizing scooters
	2. Any available scooters are searched by taking lat, lng and radius and finding
	   all the bins which enclose the circle with radius and center as long and lat
	3. For all possible bins in step 2 we measure which scooters are within radius
	   and available, end result being sent back to the user
	4. Every resevration request to the scooters in step 3 try to mark them as reserved
	
	   
#*Database Usage*

	- Configured to use MySql
	- Database Model
		Scooter_Info:
		   Location
			1) Lat: <float>
			2) Lng: <flat>
		   Bin Information
		    3) bin_lng: <int>
			4) bin_lat: <int>
		   Primary Key
		    5) scooter_id
		Reserve_Scooter_Record
			Primary Key
			 1) scooter_id 
		
			

#*Rest API*

#*Application Oriented* 

	1) Reserved scooter if it is not reserved, POST :: api/v1/reserve, (id=<integer>)
	2) GET :: api/v1/available, (lat=<float>, lng=<float>, radius=<float>)

#*Testing Oriented*

	1) Define kb algorirhm parameters :: POST :: api/v1/setvar, (kb_size=<int>, kb_bin_size=<int>)
	2) Load test data with n size:: GET :: api/v1/loadfixturedata, (size=<int>)
	3) Bring back reserved scooters only: GET :: api/v1/getreserved
	4) Get scooter information: GET :: api/v1/gettest, (size=<int>)

#*Usage*

	1) make install 
		- Install virtual env and all python packages
	2) make clean
	   - Clean up venv
	3) make start-server
	   - starts localwebserver

	   





